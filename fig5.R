library(magick)
library(ggplot2)
magick_zize <- function(TARGET_FILE, plot_object, w=4, h=4)
{
    ggsave(TARGET_FILE,
           plot=plot_object,
           width=w,
           height=h,
           limit = FALSE)
    m_png <- image_trim(image_read(TARGET_FILE))
    image_write(m_png, TARGET_FILE)
    print(paste("See:", TARGET_FILE))
}
options(crayon.enabled=FALSE)
library(FrF2)
library(tidyverse)
FrF2(nfactors = 7, resolution=8,  randomize = TRUE, seed=1, replications=30) -> plan
plan  %>%  as.tibble() %>% 
     rename(H=G) %>% # New design only have 7 cases, shift
     rename(G=F) %>% # New design only have 7 cases, shift
     mutate(F=E) %>% # Add kenel parameter as off (this 1 will be 0)
     mutate(E=D) %>% # New design unifies main_ram and app_thread
     select(A, B, C, D, E, F, G, H) %>% # Reorganize
     mutate_all(function(x){return(as.integer(x)-1L)}) -> plan_csv

#Read
read_csv("data/times_full.csv", col_types=cols(
  Run = col_integer(),
  Time = col_double(),
  Flops = col_double(),
  Var = col_double()
)) -> times

read_delim("data/plan_full.csv", delim=" ", 
col_names=c("A", "B", "C", "D", "E", "F", "G", "H"),
col_types=cols(
 A = col_integer(),
 B = col_integer(),
 C = col_integer(),
 D = col_integer(),
 E = col_integer(),
 F = col_integer(),
 G = col_integer(),
 H = col_integer())) -> run10
run20 <- run10
#run20 <- bind_rows(run10, run10)
run20 %>%
mutate(Run = row_number()) %>%
select(Run, A, B, C, D, E, F, G, H) %>%
mutate(
A = ifelse(A==0,-1,1),
 B = ifelse(B==0,-1,1),
 C = ifelse(C==0,-1,1),
 D = ifelse(D==0,-1,1),
 E = ifelse(E==0,-1,1),
 F = ifelse(F==0,-1,1),
 G = ifelse(G==0,-1,1),
 H = ifelse(H==0,-1,1),
) -> configurations

configurations %>% inner_join(times, by=c("Run")) %>%
     mutate(E=F, F=G, G=H) -> final_data
final_data %>%  group_by(A, B, C, D, E, F, G) %>% 
  summarize(mean=mean(Time), max=max(Time), min=min(Time), sd=sd(Time), n=n()) -> mean_data

times %>% arrange(Run) %>%
      filter(Run>0) %>%
      .$Time -> Time

add.response(plan, Time) -> plan.res
ia_values <- IAPlot(plan.res, select=c(1, 2, 3, 4, 5, 6, 7))

ia_values %>% as.tibble() %>% 
    mutate(Factor1V = c(-1, 1, -1, 1),
           Factor2V = c(-1, -1, 1, 1)) %>%
    pivot_longer(c(-Factor1V,-Factor2V), names_to = "Factor", values_to = "Time") %>%
    separate(Factor, sep=":", into=c("Factor1", "Factor2")) %>%
    filter(Factor1=="A") -> ia_new_values
annotate_data <- data.frame(Factor1 = c("A"),
                            Factor2 = c("E"),
                            Value = c("1")
                            )
ia_new_values %>% ggplot() +
      geom_line(aes(x=Factor2V,
                    y=Time,
                    group=Factor1V,
                    color=factor(Factor1V), 
                    linetype=factor(Factor1V)
                    )) +
      scale_color_manual(values = c("red", "black"), 
                         name="Horizontal Factor Level (A):",
                         labels=c("-1","1")) +
      scale_linetype_manual(values = c("longdash", "solid"), 
                            name="Horizontal Factor Level (A):",
                            labels=c("-1","1")) +
      geom_point(aes(x=Factor2V,
                     y=Time,
                     group=Factor1V,
                     color=factor(Factor1V)),
                 shape=15, size=2) +
      theme_bw(base_size=16) +
      geom_label(data=annotate_data, aes(label=Value),
                 x=0, y=45, size=10, 
                 fill=NA,label.size=0.8, lineheight=0,
                 color="#009bff") +
      facet_grid(Factor1 ~ Factor2) +
      scale_x_continuous(breaks=c(-1, 1), labels=c("-1", "1"), expand=c(0.2, 0)) +
      scale_y_continuous(breaks=seq(41, 49, 2), expand=c(0.15, 0)) +
      ylab("Time [s]") + theme(panel.spacing = unit(0, "lines"),
                               legend.margin=margin(0,0,0,0),
                               legend.box.margin=margin(-10,-10,-10,-10)) +
      xlab("Vertical Factor level") + theme(legend.position="bottom") -> ia_all

dir.create("./img/", showWarnings = FALSE, recursive = TRUE)
TARGET_FILE <- "img/ia_all.png"
magick_zize(TARGET_FILE,
            ia_all,
            w=12, h=2.5)

ia_values %>% as.tibble() %>% 
   mutate(Factor1V = c(-1, 1, -1, 1),
          Factor2V = c(-1, -1, 1, 1)) %>%
   pivot_longer(c(-Factor1V,-Factor2V), names_to = "Factor", values_to = "Time") %>%
   separate(Factor, sep=":", into=c("Factor1", "Factor2")) %>%
   filter(Factor1!="A") -> ia_new_values

# Make triangular inferior
ia_new_values %>% mutate(V3 = Factor1V,
                         Factor1V=Factor2V,
                         F3 = Factor1,
                         Factor1=Factor2) %>%
                  mutate(Factor2V=V3,
                         Factor2=F3) %>%
                  select(-V3, F3) -> trig_inf

annotate_data <- data.frame(Factor1 = c("B", "C", "D", "E"),
                            Factor2 = c("D", "D", "B", "D"),
                            Value = c("2", "2", "3", "4"),
                            y = c(44.6, 44.6, 44.6, 45.2)
                            )

bind_rows(ia_new_values,trig_inf) %>%  ggplot() +
      geom_line(aes(x=Factor2V,
                    y=Time,
                    group=Factor1V,
                    color=factor(Factor1V), 
                    linetype=factor(Factor1V)
                    )) +
      scale_color_manual(values = c("red", "black"), 
                         name="Horizontal Factor Level:",
                         labels=c("-1","1")) +
      scale_linetype_manual(values = c("longdash", "solid"), 
                            name="Horizontal Factor Level:",
                            labels=c("-1","1")) +
      geom_point(aes(x=Factor2V,
                     y=Time,
                     group=Factor1V,
                     color=factor(Factor1V)),
                 shape=15, size=2) +
      theme_bw(base_size=16) +
      geom_label(data=annotate_data, aes(label=Value,y=y),
                 x=0,  size=8, 
                 fill=NA,label.size=0.8, lineheight=0,
                 color="#009bff") +
                  facet_grid(Factor1 ~ Factor2) +
    scale_x_continuous(breaks=c(-1, 1), labels=c("-1", "1"), expand=c(0.2, 0)) +
    scale_y_continuous(breaks=seq(44, 46, 0.5), expand=c(0.15, 0)) +
      ylab("Time [s]") + theme(panel.spacing = unit(0, "lines"),
                               legend.margin=margin(0,0,0,0),
                               legend.box.margin=margin(-10,-10,-10,-10)) +
    xlab("Vertical Factor level") + theme(legend.position="bottom") -> ia_zoom

dir.create("./img/", showWarnings = FALSE, recursive = TRUE)
TARGET_FILE <- "img/ia_zoom.png"
magick_zize(TARGET_FILE,
            ia_zoom,
            w=12, h=7)
